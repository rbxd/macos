#!/bin/sh

wasi_path=/usr/local/Cellar/llvm/13.0.0_2/lib/clang/13.0.0/lib

brew_deps() {

    echo "\n${bold}-> Ok, I'll use brew to grab some dependencies${normal}"
    brew install llvm mercurial wget yasm
    echo '\nexport PATH="/usr/local/opt/llvm/bin:$PATH"' >> ~/.zshrc
    source ~/.zshrc
    echo "\n${bold}-> Set LLVM path${normal}"


    # aarch64 requires some extra steps
    if [ $(uname -p) == 'arm' ]; then
        brew install rustup-init
        cargo install cbindgen
    fi

}

bootstrap() {

    curl https://hg.mozilla.org/mozilla-central/raw-file/default/python/mozboot/bin/bootstrap.py -O
    python3 bootstrap.py
    rm -rf bootstrap.py mozilla-unified

}

wasi_sdk() {

    echo "\n${bold}-> Fetching the wasi sdk${normal}"
    wget -q --show-progress https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-14/wasi-sdk-14.0-macos.tar.gz
    if [ $? -ne 0 ]; then exit 1; fi
    echo "\n${bold}-> Extracting the wasi sdk (might take a while)${normal}"
    tar xf wasi-sdk-14.0-macos.tar.gz
    if [ $? -ne 0 ]; then exit 1; fi
    echo "\n${bold}-> Preparing libraries${normal}"
    mkdir -p $HOME/.mozbuild/wrlb
    mkdir -p $wasi_path/wasi
    cp -r wasi-sdk-14.0/share/wasi-sysroot $HOME/.mozbuild/wrlb/wasi-sysroot
    cp -v wasi-sdk-14.0/lib/clang/13.0.0/lib/wasi/libclang_rt.builtins-wasm32.a $wasi_path/wasi/
    echo "${bold}-> LibreWolf can now include RLbox${normal}"

}

macos_sdk() {

    echo "\n${bold}-> Fetching the MacOS sdk${normal}"
    wget -q --show-progress https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX11.3.sdk.tar.xz
    echo "\n${bold}-> Extracting the MacOS sdk (might take a while)${normal}"
    tar xf MacOSX11.3.sdk.tar.xz
    mkdir -p ~/.mozbuild/macos-sdk
    echo "\n${bold}-> Copying the MacOS sdk (might take a while)${normal}"
    cp -aH MacOSX11.3.sdk ~/.mozbuild/macos-sdk/

}

# process commandline arguments and do something
done_something=0
if [[ "$*" == *brew_deps* ]]; then
    brew_deps
    done_something=1
fi
if [[ "$*" == *bootstrap* ]]; then
    bootstrap
    done_something=1
fi
if [[ "$*" == *wasi_sdk* ]]; then
    wasi_sdk
    done_something=1
fi
if [[ "$*" == *macos_sdk* ]]; then
    macos_sdk
    done_something=1
fi

if (( done_something == 0 )); then
    cat <<EOF

Check the build guide: https://gitlab.com/librewolf-community/browser/macos/-/blob/master/build_guide.md
    
EOF
    exit 1
fi

# LibreWolf macOS
- [project website](https://librewolf.net/): read some docs and discover the project;
- [faq](https://librewolf.net/docs/faq/): most of your issues might already be solved there;
- [build guide](./build_guide.md): if you want to build from source, **suggested**;
- [bsys5](https://gitlab.com/librewolf-community/browser/bsys5): to build LibreWolf using Docker;
- [homebrew](https://formulae.brew.sh/cask/librewolf#default): install using `brew install --cask librewolf`;
- [releases](https://gitlab.com/librewolf-community/browser/macos/-/releases): for both x86 and aarch64, although the second one is not tested and it could require [troubleshooting](https://gitlab.com/librewolf-community/browser/macos/-/issues/19);
- [issue tracker](https://gitlab.com/librewolf-community/browser/macos/-/issues);
  - if you ignore the pre-requisites and the template issues might be closed;
  - issues that have the `provide info` label need user input or they will be quarantined after a week,
and closed after ten days;
- [source repository](https://gitlab.com/librewolf-community/browser/source);
- [settings repository](https://gitlab.com/librewolf-community/settings);
- join us on [gitter](https://gitter.im/librewolf-community/librewolf) / [matrix](https://matrix.to/#/#librewolf:matrix.org) / [lemmy](https://lemmy.ml/c/LibreWolf/) / [reddit](https://www.reddit.com/r/LibreWolf/)

## Notes and thanks
the main thanks go to:
- @stanzabird : made the source repo and simplified the build process by also writing the base for the current build script and bsys5;
- @ohfp : founder of the project and linux mantainer. also makes a good portion of the patches we use;
- @maltejur : huge contributor to the source repo, bsys5 and the patches;
- @brightly-salty : created the brew cask;
- [arkenfox@github](https://github.com/arkenfox): golden standard project for research on firefox prefs, the shared knowledge helps me maintaining the settings of librewolf;
- [elrumo@github](https://github.com/elrumo): made the new icon, consider a [donation](https://www.paypal.com/donate/?hosted_button_id=5PMNX4DPW83KN);
- [mozilla](https://www.mozilla.org/en-US/): provides the source code that we work on top of and helps fighting the chromium monopoly;
- [homebrew](https://brew.sh/): provides a friendly install method on osx;
- everyone willing to contribute to this project future, past, present;
- the loyal machines that built librewolf;

## License
Mozilla Public License Version 2.0. See `LICENSE.txt` for details.

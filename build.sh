#!/bin/sh

# base version
pkgver=110.0.1
# release number in the source repo, to fetch the source code
release=1
# target arch, either x86_64 or aarch64
arch=x86_64

# see the pre-requisites first
wasi_path=/usr/local/Cellar/llvm/13.0.0_2/lib/clang/13.0.0/lib

objdir=obj-*/dist/librewolf
ospkg=app
bold=$(tput bold)
normal=$(tput sgr0)

artifact_filename="librewolf-$pkgver-$release.source.tar.gz"
artifact_download_url="https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/$artifact_filename?job=Build"
source_directory="librewolf-$pkgver-$release"

prepare_source() {
    # cleaning
    echo "[debug] cleaning.."
    rm -rf "$artifact_filename" "$source_directory" librewolf-$pkgver

    # fetch
    echo "${bold}-> Fetching LibreWolf source code${normal}"
    wget -q --show-progress -O "$artifact_filename" "$artifact_download_url"
    if [ $? -ne 0 ] || [ ! -f "$artifact_filename" ]; then
        echo "Failed to fetch source tarball from $artifact_download_url" >&2
        exit 1
    fi

    echo "${bold}-> Retrieved $artifact_filename ${normal}"

    # extract
    echo "${bold}-> Extracting $artifact_filename (might take a while)${normal}"
    mkdir -p "$source_directory"
    tar xf "$artifact_filename" -C "$source_directory" --strip-components 1

    if [ $? -ne 0 ] || [ ! -d "$source_directory" ]; then
        echo "Failed to unpack source tarball" >&2
        exit 1
    fi

    echo "${bold}-> Extracted successfully ${normal}"

    # configure
    echo "${bold}-> Configuring the build${normal}"
    cd "$source_directory"

    # osx specific stuff
    if ([[ -d ~/.mozbuild/macos-sdk/MacOSX11.3.sdk ]])
    then
        echo "ac_add_options --with-macos-sdk=$HOME/.mozbuild/macos-sdk/MacOSX11.3.sdk" >> mozconfig
        echo "${bold}-> Using SDK from .mozbuild${normal}"
    fi
    cd ..

    # xcomp() - just run it after this function.

}

xcomp() {

    # to build for M1 on x86
    cd "$source_directory"
    echo "ac_add_options --target=aarch64" >> mozconfig
    echo "${bold}-> Prepared to cross-compile${normal}"
    cd ..

}

mach_build() {

    echo "\n${bold}-> OK, let's build.${normal}\n"
    if [ ! -d "$source_directory" ]; then exit 1; fi
    cd "$source_directory"
    
    ./mach build
    if [ $? -ne 0 ]; then exit 1; fi
    echo "\n${bold}-> The build ended successfully${normal}\n"

    ./mach package
    if [ $? -ne 0 ]; then exit 1; fi
    rm $objdir/LibreWolf.$ospkg/Contents/MacOS/pingsender
    echo "${bold}-> Packaged LibreWolf${normal}"
    
    cd ..

}

build_artifacts() {

    if [ ! -d "$source_directory"/$objdir ]; then exit 1; fi
    cp -r "$source_directory"/$objdir .

    echo "\n${bold}-> Creating .zip${normal}"
    zip -qr9 librewolf-$pkgver-$release-$arch.zip librewolf

    echo "\n${bold}-> Creating .dmg${normal}\n"
    cd utils
    ./disk-image.sh $pkgver-$release $arch

}

# extra functions below

add_to_apps() {

    if [ ! -d librewolf ]; then exit 1; fi
    cp -r librewolf/* /Applications    
    echo "\n${bold}-> LibreWolf.app available in /Applications\n"

}

full_build() {
    prepare_source
    mach_build
    build_artifacts
}


# process commandline arguments and do something
done_something=0
if [[ "$*" == *prepare_source* ]]; then
    prepare_source
    done_something=1
fi
if [[ "$*" == *xcomp* ]]; then
    xcomp
    done_something=1
fi
if [[ "$*" == *mach_build* ]]; then
    mach_build
    done_something=1
fi
if [[ "$*" == *build_artifacts* ]]; then
    build_artifacts
    done_something=1
fi
if [[ "$*" == *add_to_apps* ]]; then
    add_to_apps
    done_something=1
fi
if [[ "$*" == *full_build* ]]; then
    full_build
    done_something=1
fi

if (( done_something == 0 )); then
    cat <<EOF

Build script for the OSX version of LibreWolf.
For more details check the build guide: https://gitlab.com/librewolf-community/browser/macos/-/blob/master/build_guide.md

${bold}BUILD${normal}

    ${bold}./build.sh${normal} command

${bold}BUILD COMMANDS${normal}

    ${bold}full_build${normal}
        The full build process.
    
EOF
    exit 1
fi

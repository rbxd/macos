# Build using Docker

This is the **recommended** way to build, as it allows to build inside a disposable container with just a couple commands, while also preserving your environment. This way of building is also ideal if you want to build for macOS on Linux or on Windows machines.

## Part I: getting Bsys5

Bsys5 is everything you need to build LibreWolf inside a Docker container. It works on all platforms and it allows to build for all platforms, making it very easy to use and convenient. As macOS does not have support for cgroups and containers, Docker is a bit slower, so the build might take a bit longer.

To get Bsys5 enter:
```
git clone https://gitlab.com/librewolf-community/browser/bsys5.git
```

## Part II: building

To build enter the Bsys5 directory and then start the build process.

For Intel machines the commands are:
```
cd bsys5
make docker-macos-x86_64 macos-x86_64
```

For M1 machines instead:
```
cd bsys5
make docker-macos-aarch64 macos-aarch64
```

At the end of the process you will find the `.dmg` inside the same directory. To clean the build environment you can use `make clean`.

# Build with local dependencies

This guides explains how to build LibreWolf directly on your machine, without using docker. This results in faster builds but it's not recommended for everyone, as it might clutter your environment by installing a bunch of dependencies needed to build from source.

In the first part of this guide we list the prerequisites: these steps are preliminary and should be executed only once, when building LibreWolf for the first time.
The second part of the guide instead includes the real building process using the [build script](./build.sh), which also uses our [source repository](https://gitlab.com/librewolf-community/browser/source).

If you notice errors please report them in [the relevant issue](https://gitlab.com/librewolf-community/browser/macos/-/issues/37), as this whole process has just been revisited.

Further references on how to build Firefox can be found [here](https://firefox-source-docs.mozilla.org/setup/macos_build.html).

## Part I: prerequisites
In this section we will discuss how to install the needed tools to successfully build. This process should be executed once.

This process was tested on macOS BigSur and it takes advantage of a script called [pre-requisites.sh](./pre-requisites.sh), so clone the repository and move in it, by entering:
```
git clone --recursive https://gitlab.com/librewolf-community/browser/macos
cd macos
```

We will also need [Homebrew](https://brew.sh/), so make sure you install it before starting.

#### Homebrew dependencies

Enter:
```
./pre-requisites.sh brew_deps
```
and wait till it finishes.

This will use brew to install:
- `llvm`: needed to compile the wasm sandbox, but not part of Mozilla's dependencies. Please note that to proceed the script will need to set the LLVM path to the one installed by brew.
- `mercurial`: used to grab all the rest of the dependencies directly from Mozilla.
- `wget`: used by the build script to download the source code of LibreWolf.
- `yasm`: used in the build process, but not part of Mozilla's dependencies.

On M1 powered machines it will also perform:
- `brew install rustup-init`
- `cargo install cbindgen`
as these dependencies are also missing from the one that Mozilla provides.

#### Mozilla dependencies

We will now download a bunch of dependencies directly from Mozilla.

Enter:
```
./pre-requisites.sh bootstrap
```

Once the process starts it will require to hit enter to use the default clone location, then after waiting for a while, further interactions will be required. In particular:
- We will be required to select a Firefox version that we want to build: enter `2` to select Firefox for Desktop, as it is the option which includes all required dependencies (except for `yasm` that we already installed manually), including `rust` and `cbindgen` (M1 users already installed these dependencies manually in the prerequisites). This is **important**, as it might cause your build process to fail if you do not pick the right option.
- We will be notified that the script wants to create a directory for the build tools: enter `y` to let the script create the default directory.
- We will be asked to run a configuration wizard for Mercurial: enter `n`.
- We will be asked to enable the build telemetry system: enter `n`.
- We will be asked whether we want to submit commits to Mozilla: enter `n`.

When the full process finishes, close and re-open the terminal, then enter `ls -la` and you should see a directory called `.mozbuild`.

#### Wasm sandboxing

Mozilla developed a wasm sandbox called RLBox, which is a great security feature that we want to have in the browser. In order to compile it we will need some extra dependencies.

Enter:
```
./pre-requisites.sh wasi_sdk
```
and wait for the process to finish.

#### macOS SDK

The last thing we need to do now, is to include a compatible macOS SDK as part of our build dependencies. In this case we use SDK version 11.3.

Enter:
```
./pre-requisites.sh macos_sdk
```

Once this is done the pre-requisites are over and we should be finally ready to build.

## Part II: the build script
In this section we are going to describe how to start the real build process. We are also going to dicuss one by one the various steps included in the [build script](./build.sh). This second part of the build process should only be executed when the prerequisites are sucessfully completed.

Entering `./build.sh` will give us a visual output with a list of all the available options in the script.
To build enter:
```
./build.sh full_build
```

The `full_build` option of the script file will perform the following tasks in order:
```
prepare_source
mach_build
build_artifacts
```
They can also be run singularly, by entering them after `./build.sh` instead of `full_build`, or it is possible to chain them separated by a single space. So for example we can say that `./build.sh full` is equivalent to `./build.sh prepare_source mach_build build_artifacts`.

In the rest of the guide we will focus on describing what each function of the build script does.

#### prepare_source
Fetches the source code of LibreWolf from the source repo, then applies a couple of branding changes which are specific to macOS. It also sets the build options.
#### mach_build
Builds and packages the browser.
#### build_artifacts
Generates a .zip containing the application, as well as a .dmg named with the specific release version.
#### add_to_apps
Moves LibreWolf.app in the `Applications` folder.
#### xcomp
Allows to build for `aarch64` on a `x86` machines.

## Building for aarch64 on an x86 machine

With some tweaks it is possible to succesfully build a LibreWolf version targeted at M1 machines, on an Intel based MacBook.

First of all it is necessary to install the rust target for ARM-based machines, by entering in your terminal:
```
rustup target add aarch64-apple-darwin
```
This is a **prerequisite** and it should be performed only when cross-compiling for the first absolute time.

After this, the only difference with the normal build process is that we should specify that we want to cross-compile, and we can do so by using the `xcomp` part of the build script before building.
For example a good set of instructions for this purpose would be:
```
./build.sh prepare_source xcomp mach_build build_artifacts
```

## Using Xcode for the SDK
If you do not wish to use the macOS SDK mentioned above you can install Xcode from the [App Store](https://apps.apple.com/us/app/xcode/id497799835?mt=12) and use its SDK instead.

Once the installation process is over open the terminal and enter:
```
sudo xcode-select --switch /Applications/Xcode.app
sudo xcodebuild -license
```
After this, if you enter `xcode-select -p`, it should be set to `/Applications/Xcode.app/Contents/Developer`.

**NOTE**: in some cases Xcode updates can ship with SDK versions that are not supported by Mozilla build process. In that case it will be necessary to use the SDK from the pre-requisites.
